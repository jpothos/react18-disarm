# <p align = "center">Disarm a Simple React App

<p align ="center"><a href = "https://www.buymeacoffee.com/ipothos"> <img src= "images/bmac.png" ></a></p>

<img src= "images/image.jpg">

# <p align ="center"> [Enjoy The App Here!](https://vdr.vercel.app/)

### Disarm is a Basic React Application, written most simply as the main point is to expose the benefits of using React Library to operate an optically complex UI.

### The goal is to predict the numbers behind the sealed boxes and use the best tactic for maximum scores. You can select up to 4boxes for each number draw, so plan accordingly.

### The folder demo contains the production build optimized for local deployment in the browser by simply running index.html

### I am also providing the source code, hoping it will benefit those interested in getting the logic behind the "minigame" that is a part of some other :timer: event!

� Ioannis Pothos 2023