import React from "react"
import "./disarm.css"

export default function ConsoleKeys(props) {
  const style = {
    boxShadow: props.selected
      ? "0 0 0 0 , inset calc(1.5vw) calc(2.5vw) calc(3vw) calc(-2vw) rgba(0,0,0,2.7)"
      : "0px calc(2vw) calc(2vw) rgba(20, 70, 209, 0.85)",
    borderBottom: props.selected ? "solid 0px black" : "solid calc(.3vw) black",
    borderRight: props.selected ? "solid 0px black" : "solid calc(.3vw) black",

    opacity: props.completed ? "0" : "1",
    backgroundColor: "white",
  }
  return (
    <div
      className="numberPickerBoxes"
      style={style}
      onClick={props.selectedKey}
    >
      <h2>{props.value}</h2>
    </div>
  )
}
