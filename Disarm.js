import React from "react"
import "./disarm.css"
import { nanoid } from "nanoid"
import LockerBox from "./LockerBox"
import ConsoleKeys from "./ConsoleKeys"

export default function Disarm() {
  const [lockers, setLockers] = React.useState(createLockers(18))
  const [keyConsole, setKeyConsole] = React.useState(createKeyConsole())
  const [gameResult, setGameResult] = React.useState({
    Finished: false,
    Score: 0,
    Actions: 4,
  })

  const remainingActions = lockers.filter(
    (locker) => locker.isCleared === false && locker.isSelected === false
  ).length

  React.useEffect(() => {
    const allCompleted = lockers.every((locker) => locker.isCleared === true)
    if (allCompleted === true) {
      setGameResult((prev) => {
        return { ...prev, Finished: !prev.Finished }
      })
    }
  }, [lockers])

  function createLockers(x) {
    let thelockers = []
    let temp = []
    for (let i = 0; i < x; i++) {
      temp[i] = createSingleLocker()
      if (
        thelockers.filter((locker) => locker.value === temp[i].value).length >=
        4
      ) {
        i -= 1
      } else {
        thelockers.push(temp[i])
      }
    }
    return thelockers
  }

  function createSingleLocker() {
    return {
      value: Math.floor(Math.random() * 9 + 1),
      isCleared: false,
      isSelected: false,
      isFound: false,
      id: nanoid(),
    }
  }

  function selectBox(x) {
    const activeLocker = lockers.find((locker) => locker.id === x)
    if (activeLocker.isCleared === false) {
      if (activeLocker.isSelected === false && gameResult.Actions >= 1) {
        let newActions = gameResult.Actions - 1
        setGameResult((old) => ({ ...old, Actions: newActions }))
        setLockers((old) =>
          old.map((old) => ({
            ...old,
            isSelected: old.id === x ? true : old.isSelected,
          }))
        )
      } else if (activeLocker.isSelected === true) {
        let newActions = gameResult.Actions + 1
        setGameResult((old) => ({ ...old, Actions: newActions }))
        setLockers((old) =>
          old.map((old) => ({
            ...old,
            isSelected: old.id === x ? false : old.isSelected,
          }))
        )
      }
    }
  }

  const lockerPlacement = lockers.map((locker) => (
    <LockerBox
      value={locker.value}
      isCleared={locker.isCleared}
      isSelected={locker.isSelected}
      isFound={locker.isFound}
      key={locker.id}
      selectBox={() => selectBox(locker.id)}
    />
  ))

  function createKeyConsole() {
    const totalKeys = []
    for (let i = 1; i <= 9; i++) {
      totalKeys.push({
        value: i,
        selected: false,
        completed: false,
        id: nanoid(),
      })
    }
    return totalKeys
  }

  const consoleKeyPlacement = keyConsole.map((key) => (
    <ConsoleKeys
      value={key.value}
      selected={key.selected}
      completed={key.completed}
      key={key.id}
      selectedKey={() => selectedKey(key.id)}
    />
  ))

  function selectedKey(x) {
    setKeyConsole((old) =>
      old.map((old) => {
        if (old.id === x) {
          if (old.completed === true) {
            return old
          } else {
            return { ...old, selected: !old.selected }
          }
        } else {
          return { ...old, selected: false }
        }
      })
    )
  }
  function lockerRun() {
    if (gameResult.Finished === true) {
      setLockers(createLockers(18))
      setKeyConsole(createKeyConsole)
      setGameResult({ Finished: false, Score: 0, Actions: 4 })
    } else {
      let targetValue = keyConsole.find((key) => key.selected === true).value
      let score = gameResult.Score
      let found = 0
      for (let i = 0; i < lockers.length; i++) {
        if (
          lockers[i].isSelected === true &&
          lockers[i].value === targetValue
        ) {
          score += 1
          found += 1
        }
      }

      setGameResult((old) => ({
        ...old,
        Score: score,
        Actions: found === 0 ? old.Actions : old.Actions + found,
      }))
      setLockers((old) =>
        old.map((old) => {
          if (targetValue === old.value && old.isSelected === true) {
            return { ...old, isCleared: true, isSelected: false, isFound: true }
          } else if (targetValue === old.value) {
            return { ...old, isCleared: true, isSelected: false }
          } else {
            return { ...old }
          }
        })
      )
      setKeyConsole((old) =>
        old.map((old) => {
          if (targetValue === old.value) {
            return { ...old, selected: false, completed: true }
          } else {
            return { ...old }
          }
        })
      )
    }
  }

  return (
    <div className="disarm-main">
      <h1>
        {" "}
        Total Score : {gameResult.Score}{" "}
        {remainingActions >= 1 && gameResult.Finished === false
          ? `You can mark ${Math.min(
              remainingActions,
              gameResult.Actions
            )} more Sealed Boxes`
          : gameResult.Finished === true
          ? ""
          : `You can't mark more Sealed Boxes`}
      </h1>
      <div className="panel">
        <div className="center">
          <div className="locker">{lockerPlacement}</div>
        </div>
        <div className="panels">
          <div className="numberPicker">{consoleKeyPlacement}</div>
          <div className="buttonArea">
            {!keyConsole.every((key) => key.selected === false) &&
              gameResult.Finished === false && (
                <button
                  className="runCheckBtn"
                  onClick={lockerRun}
                >
                  Unseal{" "}
                  {
                    lockers.filter(
                      (locker) =>
                        locker.value ===
                        keyConsole.find((key) => key.selected === true).value
                    ).length
                  }{" "}
                  {lockers.filter(
                    (locker) =>
                      locker.value ===
                      keyConsole.find((key) => key.selected === true).value
                  ).length !== 1
                    ? "Boxes"
                    : "Box"}{" "}
                  with number :{" "}
                  {keyConsole.find((key) => key.selected === true).value}
                </button>
              )}
            {gameResult.Finished === true && (
              <button
                className="runCheckBtn"
                onClick={lockerRun}
              >
                Luck Detector Completed!Press to Restart
              </button>
            )}
          </div>
        </div>
      </div>
      {gameResult.Finished !== true && (
        <ul className="results">
          <li>There are 18 Randomly Generated Sealed Boxes</li>
          <li>
            Each time you can mark up to 4 and pick the number on the right you
            want to unseal
          </li>
          <li>
            1 point is awarded for each marked Box that contains the picked
            number
          </li>
          <li>At the End you will get your Luck Result!</li>
        </ul>
      )}
      {gameResult.Finished === true && gameResult.Score < 9 && (
        <h2 className="results">
          "I have a feeling that today is just not your day. You might want to
          avoid black cats, walking under ladders, and any other unlucky
          activities. Better safe than sorry!"
        </h2>
      )}
      {gameResult.Finished === true &&
        gameResult.Score >= 9 &&
        gameResult.Score <= 12 && (
          <h2 className="results">
            "Well, it's not the luckiest day of your life, but it's not the
            unluckiest either. Hang in there and maybe some good fortune will
            come your way."
          </h2>
        )}
      {gameResult.Finished === true &&
        gameResult.Score >= 13 &&
        gameResult.Score <= 16 && (
          <h2 className="results">
            "It's your lucky day, and it seems like the universe is conspiring
            to bring you all the good fortune in the world. Embrace it and let
            the good times roll!"
          </h2>
        )}
      {gameResult.Finished === true && gameResult.Score >= 17 && (
        <h2 className="results">
          "Whoa, talk about extreme luck! You're practically overflowing with
          good fortune today. You're winning at every game, finding money
          everywhere you go, and even getting free stuff. It's almost too good
          to be true. I'm starting to wonder if you've got some kind of cheat
          code or something... but hey, whatever works, right?"
        </h2>
      )}
    </div>
  )
}
