import React from "react"
import "./disarm.css"

export default function LockerBox(props) {
  const styles = {
    boxShadow: props.isSelected
      ? "0 0 0 0 , inset calc(1.5vw) calc(2.5vw) calc(3vw) calc(-2vw) rgba(0,0,0,2.7)"
      : "calc(1vw) calc(2vw) calc(2vw) rgba(30, 117, 231, 0.85)",
    borderTop: props.isSelected ? "solid calc(.3vw) black" : "solid 0px",
    borderLeft: props.isSelected ? "solid calc(.3vw) black" : "solid 0px",
    borderBottom: props.isSelected
      ? "solid 0px black"
      : "solid calc(.5vw) black",
    borderRight: props.isSelected
      ? "solid 0px black"
      : "solid calc(.5vw) black",
    backgroundColor: props.isCleared
      ? props.isFound
        ? "yellow"
        : "red"
      : "#59E391",
  }
  return (
    <div
      className="lockerboxes"
      style={styles}
      onClick={props.selectBox}
    >
      <h2>{props.isCleared ? props.value : "Sealed"}</h2>
    </div>
  )
}
